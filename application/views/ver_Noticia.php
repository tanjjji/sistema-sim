


<div id="homebody">
<div class="row-fluid">

<?php
    if($this->session->flashdata('editado') == TRUE){
        echo '<div class="alert alert-success alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Successo!</strong> Noticia alterada com sucesso </div>'; 
    }   
?>

<table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th class="">ID</th>
            <th class="">Titulo</th>
            <th class="">Texto</th>
            <th class="">Data</th>
            <th class="">Editar</th>
        </tr>
    </thead>
    <tbody>
  
    <?php
        foreach ($noticias as $item){
        echo '<tr>';   
        echo '<td style="text-align:center;" class="" id="id">'.$item->id.'</td>';
        echo '<td class="" id="titulo">'.$item->titulo.'</td>';  
        echo '<td style="text-align:center;" class="" id="texto">'.$item->texto.'</td>';
        echo '<td style="text-align:center;" class="" id="data">'.$item->data.'</td>';
        echo '<td style="text-align:center;">';
        echo '<button class="btn btn-success" data-toggle="modal" data-target="#exampleModal" data-id="'.$item->id.'"data-titulo="'.$item->titulo.'" data-texto="'.$item->texto.'" data-data="'.$item->data.'"data-tipo="'.$item->noticia_tipo.'" >Editar</button>';
        echo '</td>';
        echo '</tr>';
        } 
    ?>   
    </tbody>
</table>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Editar Noticia</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <form method="POST" action='editar' id='editarNoticia'>
                <div class="form-group">
                    <label for="recipient-name" class="form-control-label">ID:</label>
                    <input type="text" class="form-control" id="id" name="id" readonly="readonly" style="max-width:300px;"/>
                </div>

                <div class="form-group">
                    <label for="recipient-name" class="form-control-label">Titulo:</label>
                    <input type="text" class="form-control" name ="titulo" id="recipient-name" style="max-width:300px;">
                </div>

                <div class="form-group">
                    <label for="data">Data:</label>
                    <input type="text" class="form-control" id="data" style="max-width:300px;" name = "data" value ="<?php echo date("d/m/Y H:i");?>" readonly="readonly">
                </div>

                <div class="form-group">
                    <label for="message-text" class="form-control-label">Texto:</label>
                    <textarea class="form-control" name="texto" id="texto" style="min-height:300px"></textarea>
                </div>

                <div class="form-group">
                    <label for="exampleSelect1">Tipo da noticia</label>
                    <select class="form-control" id="tipo" name="tipo">
                        <?php
                            foreach($tipos as $item){
                                echo '<option value="'.$item->id.'">'.$item->nome_tipo.'</option>';
                            }
                        ?> 
                    </select>
                </div>

                <div class="form-group">
                    <label for="exampleInputFile">Inserir imagem:</label>
                    <input type="file" class="form-control-file" id="exampleInputFile" aria-describedby="fileHelp" name="imagem" disabled>
                    <small id="fileHelp" class="form-text text-muted">ainda desativado desenvolvento tratamento da imagem</small>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secundary" data-dismiss="modal">Cancelar</button>
                    <button class="btn btn-danger" formatcion="deletar">Deletar</button>
                    <button type="submit" class="btn btn-primary">Confirmar</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#exampleModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal

        var recipient = button.data('id') 
        var recipienttitulo = button.data('titulo')
        var recipienttexto = button.data('texto') 
        var recipientdata = button.data('data') 
        var recipienttipo = button.data('tipo') 

        var modal = $(this)
        //modal.find('.modal-title').text('Editar noticia')
        modal.find('#id').val(recipient);
        modal.find('#recipient-name').val(recipienttitulo);
        modal.find('#data').val(recipientdata)
        modal.find('#texto').val(recipienttexto)
    })
</script>