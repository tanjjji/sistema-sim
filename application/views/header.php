
<div class="nav-side-menu">
  <div class="brand">Area Administrador SIM</div>
  <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>
  
  <div class="menu-list">

    <ul id="menu-content" class="menu-content collapse out">
      <li>
        <a href="<?= base_url() ?>">
          <i class="fa fa-home fa-lg"></i> Início
        </a>
      </li>

      <li data-toggle="collapse" data-target="#noticia" class="collapsed">
        <a href="#"><i class="fa fa-calendar fa-lg"></i> Noticias <span class="arrow"></span></a>
      </li>  
      <ul class="sub-menu collapse" id="noticia">
        <a href="<?= base_url('Noticia/cadastrar') ?>"><li>Cadastrar Noticia</li></a>
        <a href="<?= base_url('Noticia/visualizar') ?>"><li>Ver Noticias</li></a>
      </ul>

      <li data-toggle="collapse" data-target="#institucionais" class="collapsed">
        <a href="#"><i class="fa fa-calendar fa-lg"></i> Institucional <span class="arrow"></span></a>
      </li>  
      <ul class="sub-menu collapse" id="institucionais">
        <a href="#"><li>Cadastrar Institucionais</li></a>
        <a href="#"><li>Ver Institucionais</li></a>
      </ul>


      <li>
        <a href="<?= base_url('Admin/logout') ?>">
          <i class="fa fa-sign-out"></i> Sair
        </a>
      </li>

    </ul>
  </div>
</div>