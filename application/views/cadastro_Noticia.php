<div id="homebody">
	<div class="alinhado-centro borda-base espaco-vertical">
		<h3>Cadastrar nova noticia</h3>
		<p>Use o formulário abaixo para cadastrar uma nova noticia.</p>
		<?php
			if($this->session->flashdata('sucesso') == TRUE){
				echo '<div class="alert alert-success alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Successo!</strong> Noticia cadastrada com sucesso </div>'; 
			}	
		?>
	</div>
    <div class="row-fluid">
    	<form id='adicionarNoticia' action='adicionar' method='post'>
    		<div class="form-group">
      			<label for="usr">Titulo:</label>
      			<input type="text" class="form-control" id="titulo" style="max-width:300px;" placeholder="Titulo" name = "titulo">
      			<small id="tituloHelp" class="form-text text-muted">Recebera maior destaque em buscadores</small>
    		</div>

            <div class="form-group">
                <label for="usr">Data:</label>
                <input type="text" class="form-control" id="data" style="max-width:300px;" placeholder="Titulo" name = "data" value ="<?php echo date("d/m/Y H:i");?>" disabled>
            </div>
      		<div class="form-group">
        		<label for="texto">Corpo da noticia:</label>
        		<textarea class="form-control" id="texto" name = "texto" style="min-height: 300px;" rows="3"></textarea>
      		</div>

            <div class="form-group">
                <label for="exampleSelect1">Tipo da noticia</label>
                <select class="form-control" id="tipo" name="tipo">
                <?php
                    foreach($tipos as $item){
                        echo '<option value="'.$item->id.'">'.$item->nome_tipo.'</option>';
                    }
                ?> 
                </select>
                </div>

      		<div class="form-group">
        		<label for="exampleInputFile">Inserir imagem:</label>
        		<input type="file" class="form-control-file" id="exampleInputFile" aria-describedby="fileHelp" disabled>
        		<small id="fileHelp" class="form-text text-muted">ainda desativado desenvolvento tratamento da imagem</small>
      		</div>
      		<input type="submit" class="btn btn-primary" value='confirmar' />
    	</form>
    </div>
</div>