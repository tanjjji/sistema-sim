<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Noticia_model extends CI_Model{
	public $id;
	public $titulo;
	public $texto;
	public $data;

	public function __construct(){
		parent::__construct();
	}

	public function listar_noticias($filtro = 'id'){
		$this->db->order_by($filtro,'ASC');
		return $this->db->get('noticia')->result();
	}

	public function detalhes_noticia($id){
		$this->db->where('id',$id);
		return $this->db->get('noticia')->result();
	}

	public function listar_tipos_noticia($sort = 'id', $order = 'asc') {
    	$this->db->order_by($sort, 'ASC');
    	return $this->db->get('noticia_tipo')->result();
  	}

  	public function update_noticia($id, $data){
  		$this->db->where('id', $id);  
		$this->db->update('noticia', $data);
  	}

  	public function deleta_noticia($id){
		$this->db->where('student_id', $id);
		$this->db->delete('students');
	}

}