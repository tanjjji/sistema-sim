<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function index(){
		if ($this->session->userdata('logado') == TRUE){
			$this->load->view('html_header');
			$this->load->view('header');
			$this->load->view('footer');
			$this->load->view('html_footer');
		}else{
			$this->form_login();
		}
	}

	public function form_login(){
		$this->load->view('loginAdmin');
	}

	public function login() {
		$this->load->library('form_validation');
    	$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
    	$this->form_validation->set_rules('senha', 'Senha', 'required|min_length[4]', array('required' => 'Você deve preencher a %s.'));

    	$email = $this->input->post('email');
		$senha = $this->input->post('senha');

    	if ($this->form_validation->run() == FALSE) {
        	$erros = array('mensagens' => validation_errors());
        	$this->session->set_flashdata('erros', $erros);
        	redirect(base_url());
     	} else {
     		$senha = $this->crypt($senha);
     		$this->load->model("login_model");
   			$usuario = $this->login_model->autenticacao($email,$senha);
   			if($usuario){
                $this->session->set_userdata("logado", TRUE);
            	$this->session->set_userdata("email_logado", $email);
            	$this->load->view('html_header');
            	$this->load->view('header');
            	//$this->load->view('admin-principal');
            	$this->load->view('html_footer'); 	
        	}else{
        		$this->session->set_userdata("logado", FALSE);
        		$this->session->set_userdata("email_logado",NULL);
            	$erros = array("erros" => "E-mail ou Senha incorretos");
            	$this->session->set_flashdata('erros', $erros);
            	redirect(base_url());
        	}
        	
    	}
 	}

    public function logout(){
        $this->session->sess_destroy();
        redirect(base_url());
    }

 	 public function crypt ($senha){
 		$senhaCrypt = md5($senha);
 		return $senhaCrypt; 
 	}

}