<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Noticia extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('Noticia_model','modelnoticias');
	}

	public function cadastrar(){
		$this->load->model('Noticia_model','modelnoticias');
		$dados['tipos'] = $this->modelnoticias->listar_tipos_noticia();
		$this->load->view('html_header');
		$this->load->view('header');
		$this->load->view('cadastro_Noticia',$dados);
		$this->load->view('footer');
		$this->load->view('html_footer');
	}

	public function editar(){
		$this->load->library('form_validation');
		$this->load->model('Noticia_model','modelnoticias');
		$this->form_validation->set_rules('titulo','titulo','required');
		$this->form_validation->set_rules('texto','texto','required');

		$id = $this->input->post('id');
		$dados['titulo'] = $this->input->post('titulo');
		$dados['texto'] = $this->input->post('texto');
		$dados['data'] = $this->input->post('data');
		$dados['noticia_tipo'] = $this->input->post('tipo');
		$dados['imagem_id'] = 1; //alterar depois do tratamento de imagem 
		$dados['imagem_imagem_galeria_id'] = 1; // alterar depois do tratamento de imagem

		if ($this->form_validation->run() == FALSE){
			$this->visualizar();
		}
		else{
			if ($this->modelnoticias->update_noticia($id,$dados) == FALSE){
				$this->session->set_flashdata('editado', FALSE);
			}else{
				$this->session->set_flashdata('editado', TRUE);
			}
			$this->visualizar();
		}
	}

	public function resume( $var, $limite ){   
        if (strlen($var) > $limite) {       
            $var = substr($var, 0, $limite);
            $var = trim($var) . "...";  
        }
            return $var;
    }

	public function visualizar(){
		$this->load->model('Noticia_model','modelnoticias');
		$dados['noticias'] = $this->modelnoticias->listar_noticias(); 
		$dados['tipos'] = $this->modelnoticias->listar_tipos_noticia();
		$dados['editado'] = 
		$this->load->view('html_header');
		$this->load->view('header');
		$this->load->view('ver_Noticia',$dados);
		$this->load->view('footer');
		$this->load->view('html_footer');
	}

	public function deletar($id){
		$id = $this->input->post('id');
		var_dump($id);
		if ($this->modelnoticias->deleta_noticia($id) == FALSE){
			$this->session->set_flashdata('deletado', FALSE);
		}else{
			$this->session->set_flashdata('deletado', TRUE);
		}
		//$this->visualizar();
	}

	public function adicionar(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('titulo','titulo','required');
		$this->form_validation->set_rules('texto','texto','required');

		$dados['titulo'] = $this->input->post('titulo');
		$dados['texto'] = $this->input->post('texto');
		$dados['data'] = $this->input->post('data');
		$dados['noticia_tipo'] = $this->input->post('tipo');
		$dados['imagem_id'] = 1; 
		$dados['imagem_imagem_galeria_id'] = 1; 

		if($this->form_validation->run() == FALSE){
			$this->cadastrar();
		} else {
			if($this->db->insert('noticia',$dados) == FALSE){
				$this->session->set_flashdata('sucesso', FALSE);
			}else{
				$this->session->set_flashdata('sucesso', TRUE);
			}
			$this->load->view('html_header');
			$this->load->view('header');
			$this->load->view('cadastro_Noticia');
			$this->load->view('footer');
			$this->load->view('html_footer');
		}
	}

}